<?php
	
	 // Databaskoppling odeon.nu
	$strServer = "localhost";											// Sökväg till din MySQL server. Ofta localhost för egna servrar. Om du har webbhotell kolla med ditt webbhotell.
	$strAnvId = "anv";													// Användarnamn för MySQL servern.
	$strLosenord = "pass";												// Lösenord för MySQL servern.
	$strDatabas = "db"; 												// Namnet på databasen som används för att lagra chaplins föreställningar.
	$webbokning = "http://odeonbio.se/chap/api/"; // Url till webbokningens API. Tänk på att Nutid måste vitlista din webbservers IP.
	$pluginsokvag = "/home/u/u2825598/www/odeon/wp-content/plugins/"; 	// Serverns lokala sökväg till pluginsmappen
	
	// Här under gör man normalt inga ändringar

	$db = new mysqli($strServer, $strAnvId, $strLosenord, $strDatabas);
	
	$now = date("Y-m-d");
	
	if ($db->connect_errno) {
    // The connection failed. What do you want to do? 
    // You could contact yourself (email?), log the error, show a nice page, etc.
    // You do not want to reveal sensitive information

    // Let's try this:
    echo "Sorry, this website is experiencing problems.";

    // Something you should not do on a public site, but this example will show you
    // anyways, is print out MySQL error related information -- you might log this
    echo "Error: Failed to make a MySQL connection, here is why: \n";
    echo "Errno: " . $db->connect_errno . "\n";
    echo "Error: " . $db->connect_error . "\n";
    
    // You might want to show them something nice, but we will simply exit
    exit;
}

	
	
	$json = file_get_contents($webbokning . 'getShowInfo@fromdate=' . $now);
	
	if($json === FALSE) { die; }
	
	$json = json_decode($json);

	foreach($json as $obj){
		$data = $obj->forestall;
   }
   
   foreach($data as $obj2){
		$datum 	= $obj2->VisaDatum;
		$tid 	= $obj2->VisaTid;
		$veckodag = $obj2->VeckoDag;
		$salong = $obj2->Salong;
		$lediga = $obj2->Lediga;
		$salongnr = $obj2->SalongNr;
		$forestallningtyp = $obj2->ForestallningTyp;
		$notering = $obj2->Notering;
		$filmnamn = $obj2->FilmNamn;
		$distrnamn = $obj2->DistrNamn;
		$aldersgrans = $obj2->AlderGrans;
		$bildformat = $obj2->BildFormat;
		$filmlangd = $obj2->FilmLangd;
		$filmpris = $obj2->FilmPris;
		$supplementpris = $obj2->SupplementPris;
		$filmnr = $obj2->FilmNr;
		$bgfilmnr = $obj2->BGFilmNr;
		$platsnum = $obj2->PlatsNum;
		$filmstatus = $obj2->Status;
		$prodar = $obj2->ProdAr;
		$sprak = $obj2->Sprak;
		$textning = $obj2->Text;
		$ljud = $obj2->Ljud;
		$genre = $obj2->Genre;
		$bildsokvag = $obj2->BildSokVag;
		$producent = $obj2->Producent;
		$imdburl = $obj2->IMDBUrl;
		$twitter = $obj2->Twitter;
		$facebook = $obj2->Facebook;
		$hemsida = $obj2->Hemsida;
		$handling = $obj2->Handling;
		$skadespelare = $obj2->Skadespalare;
		$iblank = $obj2->IBLank;
		$forestid = $obj2->ForestId;
		$regissor = $obj2->Regissor;
		$trailer = $obj2->TrailerURL;
				
		$SorteringData = $datum.' '.$tid.':00';
		
		//echo $SorteringData; die;
		
		
		if (checkifnew($db,$forestid) == "true")
			{
				$insertquery = "INSERT INTO `chapdata`(`VisaDatum`, `VeckoDag`, `VisaTid`, `SorteringData`, `Salong`, `Lediga`, `SalongNr`, `ForestallningTyp`, `Notering`, `FilmNamn`, `DistrNamn`, `AlderGrans`, `BildFormat`, `FilmLangd`, `FilmPris`, `SupplementPris`, `FilmNr`, `BGFilmNr`, `PlatsNum`, `FilmStatus`, `ProdAr`, `Land`, `Sprak`, `Textning`, `Ljud`, `Genre`, `BildSokVag`, `Producent`, `IMDBUrl`, `Twitter`, `Facebook`, `Hemsida`, `Handling`, `Skadespalare`, `IBLank`, `Ressigor`, `TrailerURL`, `ForestId`) VALUES ('$datum','$veckodag','$tid','$SorteringData','$salong','$lediga','$salongnr','$forestallningtyp','$notering','$filmnamn','$distrnamn','$aldersgrans','$bildformat','$filmlangd','$filmpris','$supplementpris','$filmnr','$bgfilmnr','$platsnum','$filmstatus','$prodar','$land','$sprak','$textning','$ljud','$genre','$bildsokvag','$producent','$imdburl','$twitter','$facebook','$hemsida','$handling','$skadespelare','$iblank','$ressigor','$trailer','$forestid')";	
				
				echo "<b>Lägger till ".$filmnamn."</b><br>";
				
				$insertquery = utf8_decode($insertquery);
				$insertresult = $db->query($insertquery);
				
				echo $filmnamn." är tillagd ".$datum." ".$tid."!<br><br>";
		
			
			}
		else
			{
				$updatequery = "UPDATE `chapdata` SET `VisaDatum`='$datum',`VeckoDag`='$veckodag',`VisaTid`='$tid',`SorteringData`='$SorteringData',`Salong`='$salong',`Lediga`='$lediga',`SalongNr`='$salongnr',`ForestallningTyp`='$forestallningtyp',`Notering`='$notering',`FilmNamn`='$filmnamn',`DistrNamn`='$distrnamn',`AlderGrans`='$aldersgrans',`BildFormat`='$bildformat',`FilmLangd`='$filmlangd',`FilmPris`='$filmpris',`SupplementPris`='$supplementpris',`FilmNr`='$filmnr',`BGFilmNr`='$bgfilmnr',`PlatsNum`='$platsnum',`FilmStatus`='$filmstatus',`ProdAr`='$prodar',`Land`='$land',`Sprak`='$sprak',`Textning`='$textning',`Ljud`='$ljud',`Genre`='$genre',`BildSokVag`='$bildsokvag',`Producent`='$producent',`IMDBUrl`='$imdburl',`Twitter`='$twitter',`Facebook`='$facebook',`Hemsida`='$hemsida',`Handling`='$handling',`Skadespalare`='$skadespelare',`IBLank`='$iblank',`Ressigor`='$ressigor',`TrailerURL`='$trailer',`ForestId`='$forestid' WHERE ForestId = '$forestid'";
				
			//	$loop = $loop + 1;
				
				//echo $loop."<br>".$updatequery."<br><br>";
				
				echo "<b>Uppdaterar ".$filmnamn."</b><br>";
								
				$updatequery = utf8_decode($updatequery);
								
				$updateresult = $db->query($updatequery);
				
				echo $filmnamn." ".$datum." ".$tid." är uppdaterad!<br><br>";
				
				
				
				
			}
			
			// Sorterar data i tabellen för att DISTINCT data ska tas från rätt föreställning.
				
				$sortquery = 'ALTER TABLE chapdata ORDER BY VisaDatum';		
					
				$db->query($sortquery);
		
   }
	
	
function checkifnew($db,$forestid){

	$query = "SELECT id FROM chapdata WHERE forestid = '$forestid'";
		$result = $db->query($query);
		$row_cnt = $result->num_rows;
	
	if ($row_cnt == "0"){$out = "true";}else{$out = "false";}
	
	return $out;
	
	}

// Här hämtas bilder som inte redan är hämtade.

$startdate = date ("Ymd", mktime(0,0,0,date(m),date(d),date(Y)));//dagens datum YYYYMMDD
$stopdate = date ("Ymd", mktime(0,0,0,date(m),date(d),date(Y)+1));


$query = "SELECT DISTINCT FilmNr FROM chapdata WHERE VisaDatum BETWEEN $startdate AND $stopdate ORDER BY VisaDatum, VisaTid";
	
	//echo $query; die;
		
	$result = $db->query($query);
			
	$num_rows = mysqli_num_rows($result);
			
		if($num_rows=="0"){
			echo "<font size=3><center><br><br>Just nu finns inget kommande program.<center></font><br>";
		}
		
		while($row = $result->fetch_assoc()){ // Tar fram unika föreställningar
			
	$FilmNr = $row["FilmNr"];
		
	$query2 = "SELECT * FROM chapdata WHERE FilmNr = '$FilmNr' ORDER BY id DESC LIMIT 1";
		
	$result2 = $db->query($query2);
		
	
		while($row2 = $result2->fetch_assoc()){ // Tar fram filmdata
			
			
			$BildSokVag = $row2["BildSokVag"];
			
			if ($BildSokVag != ""){
			
			echo "Bild finns<br>";

			$filnamn = $pluginsokvag.'bioprogrammet_v2/posters/'.$FilmNr.'.jpg';
			$filnamn2 = $pluginsokvag.'bioprogrammet_v2/posters/'.$FilmNr.'_low.jpg';
			

			
			$bild = file_get_contents($BildSokVag);	
			
			
			file_put_contents($filnamn, $bild);
		
			
			$im = new ImageManipulator($filnamn);
			$im->resample(500, 714); // resize to 640x480
			$im->save($filnamn2, IMAGETYPE_JPEG);				
				
				
			
			}
			
			
			} // Avslutar filmdataloop	
			
			}
			
			
// Klass för krympning av bild

class ImageManipulator
{
    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @var resource
     */
    protected $image;

    /**
     * Image manipulator constructor
     * 
     * @param string $file OPTIONAL Path to image file or image data as string
     * @return void
     */
    public function __construct($file = null)
    {
        if (null !== $file) {
            if (is_file($file)) {
                $this->setImageFile($file);
            } else {
                $this->setImageString($file);
            }
        }
    }

    /**
     * Set image resource from file
     * 
     * @param string $file Path to image file
     * @return ImageManipulator for a fluent interface
     * @throws InvalidArgumentException
     */
    public function setImageFile($file)
    {
        if (!(is_readable($file) && is_file($file))) {
            throw new InvalidArgumentException("Image file $file is not readable");
        }

        if (is_resource($this->image)) {
            imagedestroy($this->image);
        }

        list ($this->width, $this->height, $type) = getimagesize($file);

        switch ($type) {
            case IMAGETYPE_GIF  :
                $this->image = imagecreatefromgif($file);
                break;
            case IMAGETYPE_JPEG :
                $this->image = imagecreatefromjpeg($file);
                break;
            case IMAGETYPE_PNG  :
                $this->image = imagecreatefrompng($file);
                break;
            default             :
                throw new InvalidArgumentException("Image type $type not supported");
        }

        return $this;
    }
    
    /**
     * Set image resource from string data
     * 
     * @param string $data
     * @return ImageManipulator for a fluent interface
     * @throws RuntimeException
     */
    public function setImageString($data)
    {
        if (is_resource($this->image)) {
            imagedestroy($this->image);
        }

        if (!$this->image = imagecreatefromstring($data)) {
            throw new RuntimeException('Cannot create image from data string');
        }
        $this->width = imagesx($this->image);
        $this->height = imagesy($this->image);
        return $this;
    }

    /**
     * Resamples the current image
     *
     * @param int  $width                New width
     * @param int  $height               New height
     * @param bool $constrainProportions Constrain current image proportions when resizing
     * @return ImageManipulator for a fluent interface
     * @throws RuntimeException
     */
    public function resample($width, $height, $constrainProportions = true)
    {
        if (!is_resource($this->image)) {
            throw new RuntimeException('No image set');
        }
        if ($constrainProportions) {
            if ($this->height >= $this->width) {
                $width  = round($height / $this->height * $this->width);
            } else {
                $height = round($width / $this->width * $this->height);
            }
        }
        $temp = imagecreatetruecolor($width, $height);
        imagecopyresampled($temp, $this->image, 0, 0, 0, 0, $width, $height, $this->width, $this->height);
        return $this->_replace($temp);
    }
    
    /**
     * Enlarge canvas
     * 
     * @param int   $width  Canvas width
     * @param int   $height Canvas height
     * @param array $rgb    RGB colour values
     * @param int   $xpos   X-Position of image in new canvas, null for centre
     * @param int   $ypos   Y-Position of image in new canvas, null for centre
     * @return ImageManipulator for a fluent interface
     * @throws RuntimeException
     */
    public function enlargeCanvas($width, $height, array $rgb = array(), $xpos = null, $ypos = null)
    {
        if (!is_resource($this->image)) {
            throw new RuntimeException('No image set');
        }
        
        $width = max($width, $this->width);
        $height = max($height, $this->height);
        
        $temp = imagecreatetruecolor($width, $height);
        if (count($rgb) == 3) {
            $bg = imagecolorallocate($temp, $rgb[0], $rgb[1], $rgb[2]);
            imagefill($temp, 0, 0, $bg);
        }
        
        if (null === $xpos) {
            $xpos = round(($width - $this->width) / 2);
        }
        if (null === $ypos) {
            $ypos = round(($height - $this->height) / 2);
        }
        
        imagecopy($temp, $this->image, (int) $xpos, (int) $ypos, 0, 0, $this->width, $this->height);
        return $this->_replace($temp);
    }
    
    /**
     * Crop image
     * 
     * @param int|array $x1 Top left x-coordinate of crop box or array of coordinates
     * @param int       $y1 Top left y-coordinate of crop box
     * @param int       $x2 Bottom right x-coordinate of crop box
     * @param int       $y2 Bottom right y-coordinate of crop box
     * @return ImageManipulator for a fluent interface
     * @throws RuntimeException
     */
    public function crop($x1, $y1 = 0, $x2 = 0, $y2 = 0)
    {
        if (!is_resource($this->image)) {
            throw new RuntimeException('No image set');
        }
        if (is_array($x1) && 4 == count($x1)) {
            list($x1, $y1, $x2, $y2) = $x1;
        }
        
        $x1 = max($x1, 0);
        $y1 = max($y1, 0);
        
        $x2 = min($x2, $this->width);
        $y2 = min($y2, $this->height);
        
        $width = $x2 - $x1;
        $height = $y2 - $y1;
        
        $temp = imagecreatetruecolor($width, $height);
        imagecopy($temp, $this->image, 0, 0, $x1, $y1, $width, $height);
        
        return $this->_replace($temp);
    }
    
    /**
     * Replace current image resource with a new one
     * 
     * @param resource $res New image resource
     * @return ImageManipulator for a fluent interface
     * @throws UnexpectedValueException
     */
    protected function _replace($res)
    {
        if (!is_resource($res)) {
            throw new UnexpectedValueException('Invalid resource');
        }
        if (is_resource($this->image)) {
            imagedestroy($this->image);
        }
        $this->image = $res;
        $this->width = imagesx($res);
        $this->height = imagesy($res);
        return $this;
    }
    
    /**
     * Save current image to file
     * 
     * @param string $fileName
     * @return void
     * @throws RuntimeException
     */
    public function save($fileName, $type = IMAGETYPE_JPEG)
    {
        $dir = dirname($fileName);
        if (!is_dir($dir)) {
            if (!mkdir($dir, 0755, true)) {
                throw new RuntimeException('Error creating directory ' . $dir);
            }
        }
        
        try {
            switch ($type) {
                case IMAGETYPE_GIF  :
                    if (!imagegif($this->image, $fileName)) {
                        throw new RuntimeException;
                    }
                    break;
                case IMAGETYPE_PNG  :
                    if (!imagepng($this->image, $fileName)) {
                        throw new RuntimeException;
                    }
                    break;
                case IMAGETYPE_JPEG :
                default             :
                    if (!imagejpeg($this->image, $fileName, 95)) {
                        throw new RuntimeException;
                    }
            }
        } catch (Exception $ex) {
            throw new RuntimeException('Error saving image file to ' . $fileName);
        }
    }

    /**
     * Returns the GD image resource
     *
     * @return resource
     */
    public function getResource()
    {
        return $this->image;
    }

    /**
     * Get current image resource width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Get current image height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }
}
			
?>