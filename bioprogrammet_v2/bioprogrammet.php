<?php
	/*
	Plugin Name: Bioprogrammet från Chaplin
	Description: Shortcode [bio] ger aktuellt bioprogram.
	Plugin URI: https://www.blomsit.se
	Version: 1.0
	Author: Mikael Blom
	Author URI: https://www.blomsit.se
	 */
	 
	//Denna funktionen aktiveras av shortcode [bioprogrammet]
	function bioprogrammet_v2(){
	 // Läser in allmänna inställningar för Bioprogrammet.
	 include 'setup.inc.php';
	
	
	$content=$_GET['content'];
	$end=$_GET['end'];

	
	if ($end == "")
		{
			$end = date ("Ymd", mktime(0,0,0,date(m)+2,date(d)+15,date(Y)));
		}


	return movies($db,$content,$end,$event,$pluginsokvag);
	
	}
	
	
			 
	
	//Denna funktionen tar ut tillgängliga föreställningar
	function movies($db,$content,$end,$event,$pluginsokvag) {
		
	
	$startdate = date ("Ymd", mktime(0,0,0,date(m),date(d),date(Y)));//dagens datum YYYYMMDD
	$stopdate = $end;
		
	$starttime = $startdate."000000";
	$stoptime = $stopdate."235959";
	
	$out = '';
	
	
	$query = "SELECT DISTINCT FilmNr FROM chapdata WHERE SorteringData BETWEEN $starttime AND $stoptime ORDER BY SorteringData";
	
	// echo $query; die;
			
	$result = $db->query($query);
			
	$num_rows = mysqli_num_rows($result);
			
		if($num_rows=="0"){
			echo "<font size=3><center><br><br>Just nu finns inget kommande program.<center></font><br>";
		}
		
		while($row = $result->fetch_assoc()){ // Tar fram unika föreställningar
			
	$FilmNr = $row["FilmNr"];
		
	$query2 = "SELECT * FROM chapdata WHERE FilmNr = '$FilmNr' AND SorteringData BETWEEN $starttime AND $stoptime LIMIT 1";
		
	$result2 = $db->query($query2);
		
	
		while($row2 = $result2->fetch_assoc()){ // Tar fram filmdata
			
			$FilmNamn = $row2["FilmNamn"];
			//$BildSokVag = $row2["BildSokVag"];
			$FilmPris = round($row2["FilmPris"]);
			$FilmLangd = $row2["FilmLangd"];
			$AlderGrans = $row2["AlderGrans"];
			$Handling = $row2["Handling"];
			
			$posterurl = $pluginsokvag.'bioprogrammet_v2/posters/'.$FilmNr.'_low.jpg'; 
			$posterurl2 = $pluginsokvag.'bioprogrammet_v2/posters/'.$FilmNr.'.jpg'; 
			
						
			if (file_exists($posterurl))
				{$posterurl = plugins_url().'/bioprogrammet_v2/posters/'.$FilmNr.'_low.jpg';} elseif (file_exists($posterurl2)) {$posterurl = plugins_url().'/bioprogrammet_v2/posters/'.$FilmNr.'.jpg';}else {$posterurl = plugins_url().'/bioprogrammet_v2/default.png';}

			if($AlderGrans == '0'){$AlderGrans = 'Barntillåten';}elseif($AlderGrans == '-1'){$AlderGrans = 'Okänd';}elseif($AlderGrans == ''){$AlderGrans = 'För alla åldrar';}else{$AlderGrans = $AlderGrans.' år';};
			
			$out .= '	                        
		<link rel="image_src" href="'.$imagestring.'">
		<a name="'.$id.'"></a>
		
		<article class="art-post art-article programbox">
                                
                                                
                <div class="art-postcontent art-postcontent-0 clearfix"><div class="art-content-layout layout-item-0">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-1" style="width: 36%" >
        <p style="widows: 1; text-align: center;"><img width="330" src="'.$posterurl.'" alt="'.utf8_encode($FilmNamn).'"><span style="color: rgb(255, 255, 255); font-family: Dosis, Arial, Helvetica, sans-serif; font-size: 29px; font-weight: bold; line-height: 25px;"><br></span></p>
    </div><div class="art-layout-cell layout-item-1" style="width: 64%" >
       

 		<!-- Titel -->
        <p style="text-align: center;"><span style="font-family: Dosis; font-size: 36px; line-height: 25px; color: #FFFFFF; text-transform: uppercase;">'.utf8_encode($FilmNamn).'</span></p> 
        
        <p style="widows: 1; text-align: left;"><span style="font-weight: bold; color: #FFFFFF; ">Visas:</span></p>
        
        <!-- Tabell med tider och datum -->
        <table class="art-article" order="1"><tbody>
        	'.eventdata($db,$FilmNr).' 
		</tbody></table>
		 
		<!-- Längd, pris och åldersgräns -->
		<p style="widows: 1; text-align: left; ">
			 <span style="color: #37961D; "></span><span style="font-weight: bold; color: #FFFFFF; ">Pris:</span><span style="color: #FFFFFF; "> '.utf8_encode($FilmPris).'&nbsp;kr</span>
			 <br>
			 <span style="font-weight: bold; color: #FFFFFF; ">Längd</span><span style="color: #FFFFFF; ">: '.$FilmLangd.'</span>
			 <br>
			 <span style="font-weight: bold; color: #FFFFFF; ">Åldersgräns:</span>
			 <span style="color: #FFFFFF; "> '.$AlderGrans.' &nbsp;</span> &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
		<br>
			<span style="color: #D2DBE5; "><br></span><span style="color: rgb(255, 255, 255); "><a href="https://www.odeon.nu/filminfo/?id='.$FilmNr.'" >Klicka här för trailer och mer info</a></span>
		<br><br>
		</p> 
		
		';
			
			
		$out .= '
		<p style="widows: 1; text-align: left; ">
			<span style="color: #FFFFFF; ">
				'.utf8_encode(nl2br($Handling)).'
			</span>
		</p>
    </div>
    </div>
</div>
</div>
           
                                
                

</article><br>';

		
		
		} //Avslutar loopen som ger programmet
		
		} //Avslutar loopen som tar fram unika föreställningar
		
	

	mysqli_free_result($result);
	
	
	
	return $out;
	
		
	
}

// Denna funktion hämtar föreställningarna

function eventdata($db,$FilmNr){

	$StartDatum = date ("Y-m-d", mktime(0,0,0,date(m),date(d)-10,date(Y)));//dagens datum - 10 dagar YYYY-MM-DD
	
	$query2 = "SELECT * FROM chapdata WHERE FilmNr = '$FilmNr' AND SorteringData > '$StartDatum' ORDER BY VisaDatum";
		
	$result2 = $db->query($query2);
	

        		
	
		while($row2 = $result2->fetch_assoc()){ // Tar fram filmdata
			
			$VeckoDag = utf8_encode($row2["VeckoDag"]);
			$VisaDatum = utf8_encode($row2["VisaDatum"]);
			$VisaTid = utf8_encode($row2["VisaTid"]);
			$Lediga = utf8_encode($row2["Lediga"]);
			$IBLank = utf8_encode($row2["IBLank"]);
			$sortdata = utf8_encode($row2["SorteringData"]);
			
			
			
			$nu = time();
			$dbtid = $VisaDatum.' '.$VisaTid.':00';
			$dbtid = strtotime($dbtid);
			
			$NyVisaDatum = date("j/n", strtotime($VisaDatum));
			
			if($VeckoDag == 'måndag'){$VeckoDag = 'Måndag';}
			if($VeckoDag == 'tisdag'){$VeckoDag = 'Tisdag';}
			if($VeckoDag == 'onsdag'){$VeckoDag = 'Onsdag';}
			if($VeckoDag == 'torsdag'){$VeckoDag = 'Torsdag';}
			if($VeckoDag == 'fredag'){$VeckoDag = 'Fredag';}
			if($VeckoDag == 'lördag'){$VeckoDag = 'Lördag';}
			if($VeckoDag == 'söndag'){$VeckoDag = 'Söndag';}
			
			if($VisaDatum == "0000-00-00"){$NyVisaDatum = $sortdata;}
			
		
			
			if ($dbtid-3600>$nu){$bokningsstatus = '<a style="color: #9aca77" href="'.$IBLank.'">Boka biljetter</a>';}else{$bokningsstatus = '<span style="color: #FF8985">Bokning stängd</span>';}
			
			
			$out .= '<tr><td style="width: 120px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #FFFFFF;">'.$VeckoDag.' '.$NyVisaDatum.'</td><td style="width: 60px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #FFFFFF;">'.$VisaTid.'</td><td>'.$bokningsstatus.'</td></tr>'; 
        

			
			}
			
		

	
	return $out;
	
	
}

function filmdata2($db,$FilmNr){ // Mer detaljerad. För fiominfo

	$StartDatum = date ("Y-m-d", mktime(0,0,0,date(m),date(d)-10,date(Y)));//dagens datum - 10 dagar YYYY-MM-DD
	
	$query2 = "SELECT * FROM chapdata WHERE FilmNr = '$FilmNr' AND SorteringData > '$StartDatum' ORDER BY VisaDatum";
		
	$result2 = $db->query($query2);
	

        		
	
		while($row2 = $result2->fetch_assoc()){ // Tar fram filmdata
			
			$VeckoDag = utf8_encode($row2["VeckoDag"]);
			$VisaDatum = utf8_encode($row2["VisaDatum"]);
			$VisaTid = utf8_encode($row2["VisaTid"]);
			$Lediga = utf8_encode($row2["Lediga"]);
			$IBLank = utf8_encode($row2["IBLank"]);
			
			$nu = time();
			$dbtid = $VisaDatum.' '.$VisaTid.':00';
			$dbtid = strtotime($dbtid);
			
			$NyVisaDatum = date("j/n", strtotime($VisaDatum));
			
			if($VeckoDag == 'måndag'){$VeckoDag = 'Måndag';}
			if($VeckoDag == 'tisdag'){$VeckoDag = 'Tisdag';}
			if($VeckoDag == 'onsdag'){$VeckoDag = 'Onsdag';}
			if($VeckoDag == 'torsdag'){$VeckoDag = 'Torsdag';}
			if($VeckoDag == 'fredag'){$VeckoDag = 'Fredag';}
			if($VeckoDag == 'lördag'){$VeckoDag = 'Lördag';}
			if($VeckoDag == 'söndag'){$VeckoDag = 'Söndag';}

			
		
			
			if ($dbtid-3600>$nu){$bokningsstatus = '<a style="color: #9aca77" href="'.$IBLank.'">Boka biljetter, '.$Lediga.' platser lediga</a>';}else{$bokningsstatus = '<span style="color: #FF8985">Bokning stängd</span>';}
			
			
			$out .= '<tr><td style="width: 120px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #FFFFFF;">'.$VeckoDag.' '.$NyVisaDatum.'</td><td style="width: 60px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #FFFFFF;">'.$VisaTid.'</td><td>'.$bokningsstatus.'</td></tr>'; 
        

			
			}
			
			

	
	return $out;
	
	
}
//---------------------------------------------------------------------------------------------------


//Denna funktionen aktiveras av shortcode [Evenemang]
	function evenemang(){
	 // Läser in allmänna inställningar för Bioprogrammet.
	 include 'setup.inc.php';
	
	
	$content=$_GET['content'];
	$end=$_GET['end'];

	
	if ($end == "")
		{
			$end = date ("Ymd", mktime(0,0,0,date(m),date(d)+15,date(Y)+1));
		}


	return events($db,$content,$end,$event,$pluginsokvag);
	
	}
	
	
			 
	
	//Denna funktionen tar ut tillgängliga föreställningar
	function events($db,$content,$end,$event,$pluginsokvag) {
		
	
	$startdate = date ("Ymd", mktime(0,0,0,date(m),date(d),date(Y)));//dagens datum YYYYMMDD
	$stopdate = $end;
		
	$starttime = $startdate."000000";
	$stoptime = $stopdate."235959";
	
	$out = '';
	
	
	$query = "SELECT DISTINCT FilmNr FROM chapdata WHERE SorteringData BETWEEN $starttime AND $stoptime AND SalongNr='2' ORDER BY SorteringData";
	
	// echo $query; die;
			
	$result = $db->query($query);
			
	$num_rows = mysqli_num_rows($result);
			
		if($num_rows=="0"){
			echo "<font size=3><center><br><br>Just nu finns inget kommande program.<center></font><br>";
		}
		
		while($row = $result->fetch_assoc()){ // Tar fram unika föreställningar
			
	$FilmNr = $row["FilmNr"];
		
	$query2 = "SELECT * FROM chapdata WHERE FilmNr = '$FilmNr' AND SorteringData BETWEEN $starttime AND $stoptime LIMIT 1";
		
	$result2 = $db->query($query2);
		
	
		while($row2 = $result2->fetch_assoc()){ // Tar fram filmdata
			
			$FilmNamn = $row2["FilmNamn"];
			//$BildSokVag = $row2["BildSokVag"];
			$FilmPris = round($row2["FilmPris"]);
			$FilmLangd = $row2["FilmLangd"];
			$AlderGrans = $row2["AlderGrans"];
			$Handling = $row2["Handling"];
			
			$posterurl = $pluginsokvag.'bioprogrammet_v2/posters/'.$FilmNr.'_low.jpg'; 
			$posterurl2 = $pluginsokvag.'bioprogrammet_v2/posters/'.$FilmNr.'.jpg'; 
			
			if (file_exists($posterurl))
				{$posterurl = plugins_url().'/bioprogrammet_v2/posters/'.$FilmNr.'_low.jpg';} elseif (file_exists($posterurl2)) {$posterurl = plugins_url().'/bioprogrammet_v2/posters/'.$FilmNr.'.jpg';}else {$posterurl = plugins_url().'/bioprogrammet_v2/default.png';}

			if($AlderGrans == '0'){$AlderGrans = 'Barntillåten';}elseif($AlderGrans == '-1'){$AlderGrans = 'Okänd';}elseif($AlderGrans == ''){$AlderGrans = 'För alla åldrar';}else{$AlderGrans = $AlderGrans.' år';};
			
			$out .= '	                        
		<link rel="image_src" href="'.$imagestring.'">
		<a name="'.$id.'"></a>
		
		<article class="art-post art-article programbox">
                                
                                                
                <div class="art-postcontent art-postcontent-0 clearfix"><div class="art-content-layout layout-item-0">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-1" style="width: 36%" >
        <p style="widows: 1; text-align: center;"><img width="330" src="'.$posterurl.'" alt="'.utf8_encode($FilmNamn).'"><span style="color: rgb(255, 255, 255); font-family: Dosis, Arial, Helvetica, sans-serif; font-size: 29px; font-weight: bold; line-height: 25px;"><br></span></p>
    </div><div class="art-layout-cell layout-item-1" style="width: 64%" >
       

 		<!-- Titel -->
        <p style="text-align: center;"><span style="font-family: Dosis; font-size: 36px; line-height: 25px; color: #FFFFFF; text-transform: uppercase;">'.utf8_encode($FilmNamn).'</span></p> 
        
        <p style="widows: 1; text-align: left;"><span style="font-weight: bold; color: #FFFFFF; ">Visas:</span></p>
        
        <!-- Tabell med tider och datum -->
        <table class="art-article" order="1"><tbody>
        	'.filmdata($db,$FilmNr).' 
		</tbody></table>
		 
		<!-- Längd, pris och åldersgräns -->
		<p style="widows: 1; text-align: left; ">
			 <span style="color: #37961D; "></span><span style="font-weight: bold; color: #FFFFFF; ">Pris:</span><span style="color: #FFFFFF; "> '.utf8_encode($FilmPris).'&nbsp;kr</span>
			 <br>
			 <span style="font-weight: bold; color: #FFFFFF; ">Längd</span><span style="color: #FFFFFF; ">: '.$FilmLangd.'</span>
			 <br>
			 <span style="font-weight: bold; color: #FFFFFF; ">Åldersgräns:</span>
			 <span style="color: #FFFFFF; "> '.$AlderGrans.' &nbsp;</span> &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
		<br>
			<span style="color: #D2DBE5; "><br></span><span style="color: rgb(255, 255, 255); "><a href="https://www.odeon.nu/filminfo/?id='.$FilmNr.'" >Klicka här för trailer och mer info</a></span>
		<br><br>
		</p> 
		
		';
			
			
		$out .= '
		<p style="widows: 1; text-align: left; ">
			<span style="color: #FFFFFF; ">
				'.utf8_encode(nl2br($Handling)).'
			</span>
		</p>
    </div>
    </div>
</div>
</div>
           
                                
                

</article><br>';

		
		
		} //Avslutar loopen som ger programmet
		
		} //Avslutar loopen som tar fram unika föreställningar
		
	

	mysqli_free_result($result);
	
	
	
	return $out;
	
		
	
}

// Denna funktion hämtar föreställningarna

function filmdata($db,$FilmNr){

	$StartDatum = date ("Y-m-d", mktime(0,0,0,date(m),date(d)-10,date(Y)));//dagens datum - 10 dagar YYYY-MM-DD
	
	$query2 = "SELECT * FROM chapdata WHERE FilmNr = '$FilmNr' AND SorteringData > '$StartDatum' ORDER BY VisaDatum";
		
	$result2 = $db->query($query2);
	

        		
	
		while($row2 = $result2->fetch_assoc()){ // Tar fram filmdata
			
			$VeckoDag = utf8_encode($row2["VeckoDag"]);
			$VisaDatum = utf8_encode($row2["VisaDatum"]);
			$VisaTid = utf8_encode($row2["VisaTid"]);
			$Lediga = utf8_encode($row2["Lediga"]);
			$IBLank = utf8_encode($row2["IBLank"]);
			$sortdata = utf8_encode($row2["SorteringData"]);
			
			
			
			$nu = time();
			$dbtid = $VisaDatum.' '.$VisaTid.':00';
			$dbtid = strtotime($dbtid);
			
			$NyVisaDatum = date("j/n", strtotime($VisaDatum));
			
			if($VeckoDag == 'måndag'){$VeckoDag = 'Måndag';}
			if($VeckoDag == 'tisdag'){$VeckoDag = 'Tisdag';}
			if($VeckoDag == 'onsdag'){$VeckoDag = 'Onsdag';}
			if($VeckoDag == 'torsdag'){$VeckoDag = 'Torsdag';}
			if($VeckoDag == 'fredag'){$VeckoDag = 'Fredag';}
			if($VeckoDag == 'lördag'){$VeckoDag = 'Lördag';}
			if($VeckoDag == 'söndag'){$VeckoDag = 'Söndag';}
			
			if($VisaDatum == "0000-00-00"){$NyVisaDatum = $sortdata;}
			
		
			
			if ($dbtid-3600>$nu){$bokningsstatus = '<a style="color: #9aca77" href="'.$IBLank.'">Boka biljetter</a>';}else{$bokningsstatus = '<span style="color: #FF8985">Bokning stängd</span>';}
			
			
			$out .= '<tr><td style="width: 120px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #FFFFFF;">'.$VeckoDag.' '.$NyVisaDatum.'</td><td style="width: 60px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #FFFFFF;">'.$VisaTid.'</td><td>'.$bokningsstatus.'</td></tr>'; 
        

			
			}
			
		

	
	return $out;
	
	
}



// --------- Skapar filminfo ------------------------------------------

function filminfo_v2(){
	
	include 'setup.inc.php';
	
$FilmNr=$_GET['id'];

$query2 = "SELECT * FROM chapdata WHERE FilmNr = '$FilmNr' ORDER BY id DESC LIMIT 1";

//echo $query2; die;
		
	$result2 = $db->query($query2);
		
	
		while($row2 = $result2->fetch_assoc()){ // Tar fram filmdata
			
			$FilmNamn = $row2["FilmNamn"];
			$BildSokVag = $row2["BildSokVag"];
			$FilmPris = round($row2["FilmPris"]);
			$FilmLangd = $row2["FilmLangd"];
			$AlderGrans = $row2["AlderGrans"];
			$Handling = $row2["Handling"];
			$Skadespalare = $row2["Skadespalare"];
			$DistrNamn = $row2["DistrNamn"];
			$Sprak = $row2["Sprak"];
			$Textning = $row2["Textning"];
			$Genre = $row2["Genre"];
			$youtube = $row2["TrailerURL"];
			
			$posterurl = $pluginsokvag.'bioprogrammet_v2/posters/'.$FilmNr.'_low.jpg'; 
			$posterurl2 = $pluginsokvag.'bioprogrammet_v2/posters/'.$FilmNr.'.jpg'; 
						
			if (file_exists($posterurl))
				{$posterurl = plugins_url().'/bioprogrammet_v2/posters/'.$FilmNr.'_low.jpg';} elseif (file_exists($posterurl2)) {$posterurl = plugins_url().'/bioprogrammet_v2/posters/'.$FilmNr.'.jpg';}else {$posterurl = plugins_url().'/bioprogrammet_v2/default.png';}

			
			if($BildSokVag == ''){$BildSokVag = plugins_url().'/bioprogrammet_v2/default.png';}
			if($AlderGrans == '0'){$AlderGrans = 'Barntillåten';}elseif($AlderGrans == '-1'){$AlderGrans = 'Okänd';}elseif($AlderGrans == ''){$AlderGrans = 'För alla åldrar';}else{$AlderGrans = $AlderGrans.' år';};

			
			if($Textning == ""){$Textning = "Ingen text";}
			
			$out .= '	                        
		<link rel="image_src" href="'.$imagestring.'">
		<a name="'.$id.'"></a>
		
		<article class="art-post art-article programbox">
                                
                                                
                <div class="art-postcontent art-postcontent-0 clearfix"><div class="art-content-layout layout-item-0">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-1" style="width: 36%" >
        <p style="widows: 1; text-align: center;"><img width="330" src="'.$posterurl.'" alt="'.$FilmNamn.'"><span style="color: rgb(255, 255, 255); font-family: Dosis, Arial, Helvetica, sans-serif; font-size: 29px; font-weight: bold; line-height: 25px;"><br></span></p>
   
   
   
   
   
    </div><div class="art-layout-cell layout-item-1" style="width: 64%" >
       
 		<!-- Titel -->
        <p style="text-align: center;"><span style="font-family: Dosis; font-size: 36px; line-height: 25px; color: #FFFFFF; text-transform: uppercase;">'.utf8_encode($FilmNamn).'</span></p> ';
        
       if($youtube != ''){$out .= '<iframe width="612" height="340" src="https://www.youtube.com/embed/'.$youtube.'?rel=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>';}
        
       $out .= '<br><br><span style="font-weight: bold; color: #FFFFFF; ">Visas:</span>
        
        <!-- Tabell med tider och datum -->
        <table class="art-article" order="1"><tbody>
        	'.filmdata2($db,$FilmNr).' 
		</tbody></table>
		
		<!-- Längd, pris och åldersgräns -->
		<p style="widows: 1; text-align: left; ">
			 <span style="color: #37961D; "></span><span style="font-weight: bold; color: #FFFFFF; ">Pris:</span><span style="color: #FFFFFF; "> '.utf8_encode($FilmPris).'&nbsp;kr</span>
			 <br>
			 <span style="font-weight: bold; color: #FFFFFF; ">Längd</span><span style="color: #FFFFFF; ">: '.$FilmLangd.'</span>
			 <br>
			 <span style="font-weight: bold; color: #FFFFFF; ">Åldersgräns:</span>
			 <span style="color: #FFFFFF; "> '.$AlderGrans.' &nbsp;</span> <br>
			 <span style="font-weight: bold; color: #FFFFFF; ">Språk:</span>
			 <span style="color: #FFFFFF; "> '.utf8_encode($Sprak).' &nbsp;</span> <br>
			 <span style="font-weight: bold; color: #FFFFFF; ">Textning:</span>
			 <span style="color: #FFFFFF; "> '.utf8_encode($Textning).' &nbsp;</span> <br>
			 <span style="font-weight: bold; color: #FFFFFF; ">Genre:</span>
			 <span style="color: #FFFFFF; "> '.utf8_encode($Genre).' &nbsp;</span>
		<br>

		<p style="widows: 1; text-align: left;"><span style="font-weight: bold; color: #FFFFFF; ">Handling:</span><br>
		 			<span style="color: #FFFFFF; ">
				'.utf8_encode(nl2br($Handling)).'
			</span>
		</p>
		
		<p style="widows: 1; text-align: left;"><span style="font-weight: bold; color: #FFFFFF; ">Skådespelare:</span><br>
		 			<span style="color: #FFFFFF; ">
				'.utf8_encode($Skadespalare).'
			</span>
		</p>
		
		<p style="widows: 1; text-align: left;"><span style="font-weight: bold; color: #FFFFFF; ">Distrubutör:</span><br>
		 			<span style="color: #FFFFFF; ">
				'.utf8_encode($DistrNamn).'
			</span>
		</p>
		
		

        
            </div>
    </div>
</div>
</div>';
           
                                

			
}

return $out;

}

			 
//add a shortcode
add_shortcode('bio', 'bioprogrammet_v2');
add_shortcode('filminfo', 'filminfo_v2');
add_shortcode('evenemang','evenemang');


	
	?>