-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Värd: localhost
-- Tid vid skapande: 15 nov 2018 kl 00:44
-- Serverversion: 5.5.31
-- PHP-version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databas: `u2825598_d`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `chapdata`
--

CREATE TABLE `chapdata` (
  `id` int(11) NOT NULL,
  `ForestId` int(10) NOT NULL,
  `VisaDatum` date DEFAULT NULL,
  `VeckoDag` varchar(100) DEFAULT NULL,
  `VisaTid` varchar(100) DEFAULT NULL,
  `SorteringData` datetime DEFAULT NULL,
  `Salong` varchar(100) DEFAULT NULL,
  `Lediga` varchar(100) DEFAULT NULL,
  `SalongNr` varchar(100) DEFAULT NULL,
  `ForestallningTyp` varchar(100) DEFAULT NULL,
  `Notering` varchar(100) DEFAULT NULL,
  `FilmNamn` varchar(100) DEFAULT NULL,
  `DistrNamn` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `AlderGrans` varchar(100) DEFAULT NULL,
  `BildFormat` varchar(100) DEFAULT NULL,
  `FilmLangd` varchar(100) DEFAULT NULL,
  `FilmPris` varchar(100) DEFAULT NULL,
  `SupplementPris` varchar(100) DEFAULT NULL,
  `FilmNr` varchar(8) DEFAULT NULL,
  `BGFilmNr` varchar(100) DEFAULT NULL,
  `PlatsNum` varchar(100) DEFAULT NULL,
  `FilmStatus` varchar(100) DEFAULT NULL,
  `ProdAr` varchar(100) DEFAULT NULL,
  `Land` varchar(100) DEFAULT NULL,
  `Sprak` varchar(100) DEFAULT NULL,
  `Textning` varchar(100) NOT NULL,
  `Ljud` varchar(100) DEFAULT NULL,
  `Genre` varchar(100) DEFAULT NULL,
  `BildSokVag` varchar(100) DEFAULT NULL,
  `Producent` varchar(100) DEFAULT NULL,
  `IMDBUrl` varchar(100) DEFAULT NULL,
  `Twitter` varchar(100) NOT NULL,
  `Facebook` varchar(100) DEFAULT NULL,
  `Hemsida` varchar(100) DEFAULT NULL,
  `Handling` text,
  `Skadespalare` text,
  `IBLank` varchar(100) DEFAULT NULL,
  `Ressigor` varchar(100) NOT NULL,
  `TrailerURL` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `bokning` enum('on','off') NOT NULL DEFAULT 'on',
  `bokningurl` varchar(255) DEFAULT NULL,
  `pauser` tinyint(4) NOT NULL DEFAULT '0',
  `wrk_kiosk` enum('yes','no') NOT NULL DEFAULT 'yes',
  `wrk_kassa` enum('yes','no') NOT NULL DEFAULT 'yes',
  `wrk_maskin` enum('yes','no') NOT NULL DEFAULT 'yes',
  `visible_web` enum('yes','no') NOT NULL DEFAULT 'yes',
  `msg` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Chaplins filmprogram';

--
-- Index för dumpade tabeller
--

--
-- Index för tabell `chapdata`
--
ALTER TABLE `chapdata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `SorteringData` (`SorteringData`),
  ADD KEY `FilmNr` (`FilmNr`);

--
-- AUTO_INCREMENT för dumpade tabeller
--

--
-- AUTO_INCREMENT för tabell `chapdata`
--
ALTER TABLE `chapdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
